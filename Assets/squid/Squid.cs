﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Squid : MonoBehaviour
{
    // squid state
    public bool still = true;
    public bool charging = false;
    public bool charged = false;
    public bool jumping = false;
    //public bool canCharge = true; // squid can always charge

    // charge state
    public int charge_value = 0;
    public int charge_max = 6;
    public int charge_increment = 0;
    public int charge_increment_max = 7; // physics frames it takes to increment charge value

    public int jump_force_multiplier = 3;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    
    void FixedUpdate()
    {
       
        void Inputs()
        {
            string touch_phase = "";

            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Began)
                    touch_phase = "began";
                else if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved)
                    touch_phase = "stationary";
                else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                    touch_phase = "ended";
            }

            if (Input.GetKeyDown("space") || touch_phase == "began" || Input.GetMouseButtonDown(0))
            {
                still = false;
                charging = true;
                charge_value = 1;
            }
            else if (Input.GetKey("space") || touch_phase == "stationary" || Input.GetMouseButton(0))
            {
                charge_increment += 1;
                if (charge_increment == charge_increment_max)
                {
                    charge_increment = 0;
                    charge_value += 1;

                    if (charge_value > charge_max)
                    {
                        charge_value = charge_max;
                        charging = false;
                        charged = true;
                    }
                }

            }
            else if (Input.GetKeyUp("space") || touch_phase == "ended" || Input.GetMouseButtonUp(0))
            {
                gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, charge_value * jump_force_multiplier), ForceMode2D.Impulse);
                jumping = true;
                charged = false;
                charging = false;
                charge_value = 0;
                charge_increment = 0;
            }
        }


        Inputs();
        
    }

    void OnCollisionEnter2D(Collision2D c)
    {
        Debug.Log("collision entered");
        string tag = c.gameObject.tag;

        if (tag == "Ground")
        {
            // squid has hit ground, change state
            still = true;
            charging = false;
            charged = false;
            jumping = false;


        }
    }
}
